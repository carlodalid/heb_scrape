#
# HebScraper::LoginHeb
# This part of the script reads the the output file
# from HebScraper::Main and after logging in to the HEB
# website with Selenium
#
# The script then collates all product pages and prints
# them into a file
#
# File output name: final_product_pages_*current_date*.txt
#
require 'yaml'
require 'capybara'
require 'selenium-webdriver'

module HebScraper
  module LoginHeb

    def self.init!
      Capybara.default_driver = :selenium
      Capybara.javascript_driver = :selenium
      Capybara.app_host = "https://www.heb.com/"

      @credentials = YAML::load_file("config/account.yml")

      @product_links = File.open("outputs/product_pages_#{Time.now.strftime('%Y_%m_%d')}.txt", 'w')
      @selenium_session = Capybara::Session.new(:selenium)

      file_stream = File.open("outputs/group_product_pages_#{Time.now.strftime('%Y_%m_%d')}.txt", 'r')
      @group_product_pages = file_stream.readlines
    end

    def self.run!
      @selenium_session.visit '/my-account/login'
      @selenium_session.fill_in 'reg-login', :with => @credentials['credentials']['username']
      @selenium_session.fill_in 'reg-password', :with => @credentials['credentials']['password']
      @selenium_session.click_button 'Log in'

      @selenium_session.click_link 'Food & Drinks'

      @group_product_pages.each_with_index do |line, index|
        line.strip!

        if index == 0
          try_visiting_url(line)

          el = @selenium_session.first('.instore-product')
          el.click()

          first_divs = @selenium_session.all('.cat-list-deparment a')
          process_product_pages_p2(first_divs)

          next
        end

        puts "Processing products in line #{line}..."
        process_product_pages_p1(line)
      end

      puts "Done processing group product pages..."
    end

    private

    def self.process_product_pages_p1(url)
      try_visiting_url(url)
      main_divs = @selenium_session.all('.cat-list-deparment a')

      process_product_pages_p2(main_divs)

      begin
        while !@selenium_session.first('#nextPage').nil? do
          @selenium_session.find('#nextPage').click()
          sub_divs = @selenium_session.all('.cat-list-deparment a')

          process_product_pages_p2(sub_divs)
        end
      rescue Net::ReadTimeout
        puts "Encountered Net::ReadTimeout..."
        puts "Putting script to sleep for 5 secs..."
        sleep(5)

        retry
      end
    end

    def self.process_product_pages_p2(elems)
      elems.each do |elem|
        @product_links.puts(elem[:href])
      end
    end

    def self.try_visiting_url(url)
      begin
        @selenium_session.visit url
      rescue Net::ReadTimeout
        puts "Encountered Net::ReadTimeout..."
        puts "Putting script to sleep for 5 secs..."
        sleep(5)

        retry
      end
    end

  end
end

HebScraper::LoginHeb.init!
HebScraper::LoginHeb.run!
