#
# HebScraper::Main
# This part of the script reads the main_categories.yml
# and categories.yml in config folder
#
# Based on the yaml files, the script will then generate
# a file with the group product pages from Heb website
#
# File output name: group_product_pages_*current_date*.txt
#
require 'mechanize'
require 'yaml'

module HebScraper
  module Main
    HOST = 'https://www.heb.com'
    INIT_PATH = '/category/shop/'

    def self.init!
      @mechanize = Mechanize.new
      @main_categories = YAML::load_file(File.join(__dir__, 'config/main_categories.yml'))
      @sub_categories = YAML::load_file(File.join(__dir__, 'config/categories.yml'))
      @current_file = File.open("outputs/group_product_pages_#{Time.now.strftime('%Y_%m_%d')}.txt", 'w')

      @category_hash = { }
    end

    def self.run!
      @main_categories['categories'].each do |key, val|
        new_key = key.gsub(/\| |,/, '-')
        @category_hash[new_key] = val
      end

      @sub_categories.each do |sub_key, sub_category|
        category_name = sub_key.gsub(/\/| /, '-')
        category_key = @category_hash[category_name]

        sub_category.each do |sub_name, sub_num|
          sub_category_name = sub_name.gsub(/\/| /, '-').tr(',', '')

          url = "#{HOST}#{INIT_PATH}#{category_name}/#{sub_category_name}/#{category_key}/#{sub_num}"
          uri = URI.parse(url)

          response = @mechanize.get(url)
          title = response.search('.currently-showing-title')

          puts "#{category_name} >> #{sub_category_name}"
          if title.count > 0
            new_url = "#{INIT_PATH}#{category_name}/#{sub_category_name}/#{category_key}/#{sub_num}"
            puts "Product list URL: #{new_url}"

            @current_file.puts(new_url)
          else
            cat_list = response.search('.cat-list-deparment a')
            filter_group_product_pages(cat_list)
          end
        end
      end

      puts "Done scraping page categories..."
    end

    private

    def self.filter_group_product_pages(category_list)
      category_list.each do |category|
        new_url = category[:href]

        response = @mechanize.get(new_url)
        title = response.search('.currently-showing-title')

        if title.count > 0
          puts "Product list URL: #{new_url}"
          @current_file.puts(new_url)
        else
          more_categories = response.search('.cat-list-deparment a')
          filter_group_product_pages(more_categories)
        end
      end
    end

  end
end

HebScraper::Main.init!
HebScraper::Main.run!
