require 'mechanize'
require 'csv'
require 'yaml'

module HebScraper
  module ProductScraper
    HOST = 'https://images.heb.com'

    def self.init!
      @mechanize = Mechanize.new
      @csv = CSV.open("outputs/final_products_#{Time.now.strftime('%Y_%m_%d')}.csv", 'w',
        { :col_sep => ',',
          :write_headers => true,
          :headers => ['name', 'sku', 'quantity', 'price', 'category', 'subcategory', 'image']
      })

      @product_stream = File.open("outputs/product_pages_#{Time.now.strftime('%Y_%m_%d')}.txt", 'r')
    end

    def self.run!
      puts "Reading product pages..."
      @product_stream.each_line do |line|
        response = @mechanize.get line.strip!

        sku = response.search('#sku_')
        next if sku.empty?

        prod_sku          = sku.first[:value]
        prod_name         = response.search('#contentName').first[:value]
        puts "Added #{prod_name} to final products csv..."

        prod_quantity     = response.search("#packing-options_#{prod_sku}").first.xpath('text()').to_s.strip!
        prod_price        = response.search("#salePrice_#{prod_sku}").first.xpath('text()').to_s.strip!
        prod_image_link   = HOST + response.search('#contentUrl').first[:value]
        prod_category     = response.search('.breadcrumb-arrow')[1].next.next.xpath('text()').to_s.strip!
        prod_sub_category = response.search('.breadcrumb-arrow')[2].next.next.xpath('text()').to_s.strip!

        @csv << [prod_name, prod_sku, prod_quantity, prod_price, prod_category, prod_sub_category, prod_image_link]
      end

      @csv.close
    end
  end
end

HebScraper::ProductScraper.init!
HebScraper::ProductScraper.run!
