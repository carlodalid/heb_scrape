This script is customized to scrape products from https://heb.com

Notes:

1. Categories and Sub Categories are inside config folder

2. There are 3 separate scripts, below are descriptions of each script.

  * heb_scraper_part_1.rb
    - reads yaml files inside config - categories.yml and main_categories.yml
    - scrapes heb site for pages with multiple products sample below:
      - https://www.heb.com/category/shop/food-and-drinks/grocery/snacks-and-candy/dried-fruits-and-nuts/3080/3508
    - exports a file with links from of these pages and gets named group_product_pages_*current_date*.rb ie group_product_pages_01_01_2016.txt

  * heb_scraper_part_2.rb
    - probably the longest time to finish, this script simulates visiting each page/link found in the exported file from heb_scraper_part_1.rb
    - reads exported file from heb_scraper_part_1.rb and visits each link including paginated pages
    - exports product pages link to a file named final_product_pages_*current_date*.txt ie final_product_pages_01_01_2016.txt

  * heb_scraper_part_3.rb
    - reads the exported file from heb_scraper_part_2.rb and visits each product page to collect data from each product page
    - exports found data from each product page to product_pages_*current_date*.csv

3. Sample outputs are inside outputs folder without the dates suffix



How To:

1. Make sure you have Ruby installed >= 2.2.3

2. Make sure you have bundler gem installed ('gem install bundler --no-ri --no-rdoc')

3. Run bundle install ('bundle install')

4. Make sure you have outputs folder in root directory

5. Run heb_scraper_part_1.rb 'time ruby heb_scraper_part_1.rb'

6. After part_1 script finishes, run heb_scraper_part_2.rb 'time ruby heb_scraper_part_2.rb'

7. After part_2 script finishes, run heb_scraper_part_3.rb 'time ruby heb_scraper_part_3.rb'